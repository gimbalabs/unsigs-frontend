const getWalletDict = async () => { 
    return walletDict;
}

export default getWalletDict;

export const walletDict = {
    'Nami': typeof window === 'undefined' ? {} : window.cardano.nami,
    'ccvault': typeof window === 'undefined' ? {} : window.cardano.ccvault,
    'Flint': typeof window === 'undefined' ? {} : window.cardano.flint,
    'Yoroi': typeof window === 'undefined' ? {} : window.cardano.yoroi,
    'CardWallet': typeof window === 'undefined' ? {} : window.cardano.cardwallet,
    'GeroWallet': typeof window === 'undefined' ? {} : window.cardano.gerowallet
}