import { useEffect, useState } from 'react';
import { useStoreState } from "easy-peasy";

import Cardano from '../cardano/serialization-lib';
import Wallet from '../cardano/wallet';

const useWallet = (initial = null) => {
  const connected = useStoreState((state) => state.connection.connected);
  const [wallet, setWallet] = useState(initial);

  useEffect(() => {
    if (connected && wallet === null) {
      getWallet(localStorage.getItem('wallet'));
    }
  }, [connected]);

  const getWallet = async (walletName) => {
    await Cardano.load();
    await Wallet.enable(walletName);
    const walletAddress = (await Wallet.getUsedAddresses())[0];
    const walletUtxos = await Wallet.getUtxos();

    setWallet({
      address: walletAddress,
      utxos: walletUtxos,
    });
  };

  return { wallet };
};

export default useWallet;
