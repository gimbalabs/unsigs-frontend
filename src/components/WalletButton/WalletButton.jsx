import React, { useState, useEffect } from "react";
import { Link } from "gatsby"

import { navigate } from "gatsby-link";
import Cardano from "../../cardano/serialization-lib"
import Wallet from "../../cardano/wallet";

import { useStoreActions, useStoreState } from "easy-peasy";
import { Button, Box, useToast, Menu, MenuList, MenuItem, MenuButton } from "@chakra-ui/react";
import { ChevronRightIcon, ChevronDownIcon, InfoIcon } from "@chakra-ui/icons";
import { walletDict } from "../../cardano/wallet/walletDict";
// loader - see line 16 in SpaceBudz/StartButton.jsx


const WalletButton = (props) => {
    const [loading, setLoading] = useState(false);
    const [flag, setFlag] = useState(false);
    const connected = useStoreState((state) => state.connection.connected);
    const setConnected = useStoreActions((actions) => actions.connection.setConnected);
    const walletUtxos = useStoreState((state) => state.ownedUtxos.utxos)
    const setWalletUtxos = useStoreActions((actions) => actions.ownedUtxos.add)
    const toast = useToast();

    useEffect(async () => {
      const wallet = localStorage.getItem('wallet');
        if (wallet && await Wallet.enable(wallet)) {
            const utxos = await Wallet.getUtxos();
            setWalletUtxos(utxos);
        }
    }, []);

    useEffect(() => {
        if (connected && !flag) {
          const wallet = localStorage.getItem('wallet')
          if(!wallet) return;
          Wallet.enable(wallet).catch((e)=> {});
          //not implemented by some wallets yet
          // window.cardano.onAccountChange(async () => {
          //     const address = await addressToBech32();
          //     // const address = "abcd"
          //     setConnected(address);
          //     setFlag(true);
          // });
        }
    }, [connected]);

    const addressToBech32 = async () => {
      await Cardano.load();
      const address = (await Wallet.getUsedAddresses())[0];
      return address;
    };


    const connectWallet = async (wallet) => {
      setLoading(true);
      if (!(await checkStatus(toast, connected, wallet))) {
          setLoading(false);
          return;
      }
      if (await Wallet.enable(wallet).catch((e) => {})) {
          const address = await addressToBech32();
          // const address = "abcd"
          setConnected(address);
          localStorage.setItem("session", Date.now().toString());
          localStorage.setItem("wallet", wallet);
          //not implemented by some wallets yet
          // await WrongNetworkToast(toast);
      }
      setLoading(false);
    }


    const checkConnection = async () => {
      const wallet = localStorage.getItem("wallet");
      if (wallet && window.cardano && (await Wallet.enable(wallet))) {
          const session = localStorage.getItem("session");
          if(Date.now() - parseInt(session) < 6000000) {
              //1h
              const address = await addressToBech32();
              // const address = "abcd"
              setConnected(address);
          }
      }
    };

    useEffect(() => {
        checkConnection();
    }, []);

      return <>
        { connected ? <Link to="/collection">
          <Button colorScheme='teal'>
              my collection
          </Button>
        </Link> : <></>}
        <Menu>
          <MenuButton 
            isDisabled={loading}
            isLoading={loading}
            py="5"
            variant={connected ? 'ghost' : 'solid' }
            colorScheme={!connected ? 'yellow' : 'transparent' }
            as={Button} 
            rightIcon={<ChevronDownIcon />}
          > {connected ? '' :  'Connect a wallet'}
          </MenuButton>
          <MenuList textColor={'rgb(35, 33, 41)'}>
            {
              Object.keys(walletDict).map((key, index) =>
                <MenuItem key={index} onClick={async () => connectWallet(key)}>{key}</MenuItem>
              )
            }
          </MenuList>
        </Menu>
      </>
}

export default WalletButton;

const checkStatus = async (toast, connected, wallet) => {
    return (
      NoWallet(toast) 
      // &&
      // (await Wallet.enable(wallet).catch((e) => {})) &&
      // (await WrongNetworkToast(toast))
    );
};

const NoWallet = (toast) => {
    if (window.cardano) return true;
    toast({
      position: "bottom-right",
      render: () => (
        <Box
          background="purple.400"
          color="white"
          px={6}
          py={3}
          rounded="3xl"
          display="flex"
          alignItems="center"
        >
          <InfoIcon />
          <Box ml="3" fontWeight="medium">
            No wallet installed
          </Box>
          <Button
            rounded="3xl"
            colorScheme="whiteAlpha"
            onClick={() => window.open("https://namiwallet.io")}
            ml="4"
            size="xs"
            rightIcon={<ChevronRightIcon />}
          >
            Get Nami
          </Button>
          <Button
            rounded="3xl"
            colorScheme="whiteAlpha"
            onClick={() => window.open("https://ccvault.io")}
            ml="4"
            size="xs"
            rightIcon={<ChevronRightIcon />}
          >
            Get ccvault
          </Button>
        </Box>
      ),
      duration: 9000,
    });
    return false;
};

const WrongNetworkToast = async (toast) => {
    // 0 for Testnet | 1 for Mainnet
    if ((await Wallet.getNetworkId()) === 1) return true;
    toast({
      position: "bottom-right",
      duration: 5000,
      render: () => (
        <Box
          background="purple.400"
          color="white"
          px={6}
          py={3}
          rounded="3xl"
          display="flex"
          alignItems="center"
        >
          <InfoIcon />
          <Box ml="3" fontWeight="medium">
            Wrong network
          </Box>
        </Box>
      ),
    });
    return false;
};